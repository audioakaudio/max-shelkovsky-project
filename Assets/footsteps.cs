﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class footsteps : MonoBehaviour
{
    public AK.Wwise.Event footster_event;

    public RaycastHit hit;
    public float ray_distance = 0.1f;
    public LayerMask material_layer;
    public string material_value;

    void Update()
    {
        Debug.DrawRay(transform.position, Vector3.down * ray_distance, Color.red);
    }


    public void footstep()
    {
        MaterialCheck();
        AkSoundEngine.SetSwitch("Material", material_value, gameObject);
        footster_event.Post(gameObject);

    }

    public void land()
    {
        MaterialCheck();
        AkSoundEngine.SetSwitch("Material", material_value, gameObject);
        footster_event.Post(gameObject);

    }

    void MaterialCheck()
    {
        if (Physics.Raycast(transform.position, Vector3.down, out hit, ray_distance, material_layer))
        {
            if (hit.collider.tag == "Sidewalk")
                material_value = "Sidewalk";
            else if (hit.collider.tag == "Road")
                material_value = "Sidewalk";
            else if (hit.collider.tag == "Paper")
                material_value = "Paper";
            else if (hit.collider.tag == "Metal")
                material_value = "Metal";
            else if (hit.collider.tag == "Glass")
                material_value = "Sidewalk";
            else if (hit.collider.tag == "Dirt")
                material_value = "Sidewalk";

        }
    }
}
