﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class battle_music : MonoBehaviour
{
    public AK.Wwise.Event MyEvent;
    public float music_state = 0f;
    public AK.Wwise.Switch MySwitch;

    // Start is called before the first frame update
    void Start()
    {
        //MyState.SetValue(gameObject);
        MyEvent.Post(gameObject);
    }

    // Update is called once per frame
    public void CombatEnter()
    {
        AkSoundEngine.SetSwitch("Music", "Battle", gameObject);
    }

    public void CombatExit()
    {
        AkSoundEngine.SetSwitch("Music", "City", gameObject);
    }

    public void DoEvents()
    {
        AkSoundEngine.SetSwitch("Music", "City", gameObject);
    }
}
