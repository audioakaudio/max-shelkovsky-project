﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trigger_telephone : MonoBehaviour
{
    public AK.Wwise.Event trigger_event;
    public bool TriggerOnce = false;
    public bool hasTriggered = false;


    void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.tag == "Player")
        {
            if (TriggerOnce && hasTriggered)
            {
                return;
            }
            trigger_event.Post(gameObject);
            hasTriggered = true;
        }


    }
}
