﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class doppler_drone : MonoBehaviour
{
    public float SpeedOfSound = 343.3f;
    public float DopplerFactor = 1.0f;
    public GameObject player;


    Vector3 emitterLastPosition = Vector3.zero;
    Vector3 listenerLastPosition = Vector3.zero;

    // Update is called once per frame 
    void FixedUpdate()
    {

        // get the player object handy for the rest of the script! 



        // get velocity of source/emitter manually 
        Vector3 emitterSpeed = (emitterLastPosition - transform.position) / Time.fixedDeltaTime;
        emitterLastPosition = transform.position;

        // get velocity of listener/player manually 
        Vector3 listenerSpeed = (listenerLastPosition - player.transform.position) / Time.fixedDeltaTime;
        listenerLastPosition = player.transform.position;

        // do doppler calc  

        var distance = (player.transform.position - transform.position); // source to listener vector 
        var listenerRelativeSpeed = Vector3.Dot(distance, listenerSpeed) / distance.magnitude;
        var emitterRelativeSpeed = Vector3.Dot(distance, emitterSpeed) / distance.magnitude;
        listenerRelativeSpeed = Mathf.Min(listenerRelativeSpeed, (SpeedOfSound / DopplerFactor));
        emitterRelativeSpeed = Mathf.Min(emitterRelativeSpeed, (SpeedOfSound / DopplerFactor));
        var dopplerPitch = (SpeedOfSound + (listenerRelativeSpeed * DopplerFactor)) / (SpeedOfSound + (emitterRelativeSpeed * DopplerFactor));

        print(dopplerPitch);
        AkSoundEngine.SetRTPCValue("Doppler", dopplerPitch);


    }


}
