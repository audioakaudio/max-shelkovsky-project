﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class breath_stamina : MonoBehaviour
{
    public string _breath;
    public GameObject character;
    public float _stamina;
    public AK.Wwise.Event breath;
    public AK.Wwise.Event stopbreath;


    // Start is called before the first frame update
    void Start()
    {
        character = GameObject.Find("vMeleeController_Character_Junky_Female_01");
    }

    public void start_breathing()
    {
        breath.Post(gameObject);


    }

    public void stop_breathing()
    {
        stopbreath.Post(gameObject);


    }

    // Update is called once per frame
    void Update()
    {

        _stamina = character.GetComponent<vThirdPersonController>().currentStamina;
        AkSoundEngine.SetRTPCValue("Breath", _stamina);
        //print(_stamina);

    }
}
