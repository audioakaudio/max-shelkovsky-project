/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID ADVERTISING1 = 3373058746U;
        static const AkUniqueID ADVERTISING2 = 3373058745U;
        static const AkUniqueID AI_DEATH = 96636912U;
        static const AkUniqueID AMB_CITY = 4059359267U;
        static const AkUniqueID ARMOR = 662199250U;
        static const AkUniqueID AUTOMAT = 2588293218U;
        static const AkUniqueID AUTOMAT_CASH = 1930284064U;
        static const AkUniqueID BAR_MUSIC = 1155314724U;
        static const AkUniqueID BIRD_1 = 627484336U;
        static const AkUniqueID BIRD_2 = 627484339U;
        static const AkUniqueID BODYFALL = 2587856914U;
        static const AkUniqueID BREATH = 1326786195U;
        static const AkUniqueID BREATH_STOP = 2687448926U;
        static const AkUniqueID CAMERA_LEFT = 2527161000U;
        static const AkUniqueID CAMERA_RIGHT = 2474769439U;
        static const AkUniqueID CAR_IDLE = 1724060478U;
        static const AkUniqueID CAR_SIREN = 851217231U;
        static const AkUniqueID CHINESE_MUSIC = 2324877654U;
        static const AkUniqueID CLUB_MUSIC_SONG = 4103408887U;
        static const AkUniqueID DRONE = 2739838641U;
        static const AkUniqueID DRONE_FLYGHT = 418480270U;
        static const AkUniqueID ELECTRIC = 3250089732U;
        static const AkUniqueID FLY_DRONE = 1338938007U;
        static const AkUniqueID FLY_GARBAGE = 2410078456U;
        static const AkUniqueID FOOTSTEPS = 2385628198U;
        static const AkUniqueID FOOTSTEPS_AI = 2026752587U;
        static const AkUniqueID GUN_FOLEY_DOWN = 1871080784U;
        static const AkUniqueID GUN_FOLEY_UP = 3561905267U;
        static const AkUniqueID GUN_RELOAD = 1543455291U;
        static const AkUniqueID GUNSHOT = 1287408361U;
        static const AkUniqueID GUNSHOT_AI = 1431829326U;
        static const AkUniqueID HAND = 3666971986U;
        static const AkUniqueID HIT_AI = 3993554669U;
        static const AkUniqueID LEAVES = 582824249U;
        static const AkUniqueID LOCATION_MUSIC = 4054184860U;
        static const AkUniqueID METAL_BULLET_HIT = 266325583U;
        static const AkUniqueID METAL_HIT = 2473374816U;
        static const AkUniqueID NEON_SIGN1 = 352727948U;
        static const AkUniqueID NEON_SIGN2 = 352727951U;
        static const AkUniqueID NYA_CAT_SONG = 67337978U;
        static const AkUniqueID PLATFORM_ANIMATION = 2216560151U;
        static const AkUniqueID QUADCOPTER = 2386209387U;
        static const AkUniqueID ROBOTH_VOICE_PORTAL1 = 3780479660U;
        static const AkUniqueID ROBOTH_VOICE_PORTAL2 = 3780479663U;
        static const AkUniqueID START = 1281810935U;
        static const AkUniqueID STOP = 788884573U;
        static const AkUniqueID TELEPHONE = 408500223U;
        static const AkUniqueID TRIGGER_DOORS1 = 875392942U;
        static const AkUniqueID TRIGGER_DOORS2 = 875392941U;
        static const AkUniqueID VOICE = 3170124113U;
        static const AkUniqueID WINGS = 691898787U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace LOCATION
        {
            static const AkUniqueID GROUP = 1176052424U;

            namespace STATE
            {
                static const AkUniqueID BANK = 1744429087U;
                static const AkUniqueID BATTLE_MUSIC = 3832016031U;
                static const AkUniqueID CLUB = 4072605221U;
                static const AkUniqueID STREET = 4142189312U;
                static const AkUniqueID TONEL = 1940391031U;
            } // namespace STATE
        } // namespace LOCATION

    } // namespace STATES

    namespace SWITCHES
    {
        namespace MATERIAL
        {
            static const AkUniqueID GROUP = 3865314626U;

            namespace SWITCH
            {
                static const AkUniqueID DIRT = 2195636714U;
                static const AkUniqueID GLASS = 2449969375U;
                static const AkUniqueID METAL = 2473969246U;
                static const AkUniqueID PAPER = 3126687001U;
                static const AkUniqueID ROAD = 2110808655U;
                static const AkUniqueID SIDEWALK = 3137249879U;
            } // namespace SWITCH
        } // namespace MATERIAL

        namespace MUSIC
        {
            static const AkUniqueID GROUP = 3991942870U;

            namespace SWITCH
            {
                static const AkUniqueID BATTLE = 2937832959U;
                static const AkUniqueID CITY = 3888786832U;
            } // namespace SWITCH
        } // namespace MUSIC

    } // namespace SWITCHES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID AI_GUNSHOT = 1538069584U;
        static const AkUniqueID AZIMUTH = 1437246667U;
        static const AkUniqueID BREATH = 1326786195U;
        static const AkUniqueID CAMERA_HEIGHT = 587070286U;
        static const AkUniqueID DOPPLER = 4247512087U;
        static const AkUniqueID SIDECHAIN = 1883033791U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MASTER = 4056684167U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MUSIC_BUS = 3127962312U;
        static const AkUniqueID MUSIC_CLUB = 3060412054U;
        static const AkUniqueID SIDECHAIN = 1883033791U;
    } // namespace BUSSES

    namespace AUX_BUSSES
    {
        static const AkUniqueID BANK = 1744429087U;
        static const AkUniqueID CLUB = 4072605221U;
        static const AkUniqueID TONEL_REVERB = 1453492790U;
    } // namespace AUX_BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
